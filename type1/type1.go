package type1

import (
//"fmt"
)

/*
def mitronpacket(payload, unknown = "000")
  len = payload.size
  print "Length: #{len} bytes: "
  start = 0x16.chr + 0x02.chr # <SYN><STX>
  msg = "#{unknown}#{len.chr}#{payload}"
  checksum = 0x00
  msg.each_byte {|b| checksum = checksum ^ b;}
  packet = "#{start}#{msg}#{checksum.chr}#{0x3.chr}"
  packet.each_byte do |b|
    print "%02X " % b
  end
  puts
  return packet
end
*/

func MitronPacket(payload []byte, addr []byte) []byte {
	start := []byte{0x16, 0x02}
	l := byte(len(payload))
	msg := append(addr, l)
	msg = append(msg, payload...)
	checksum := byte(0)
	for _, c := range msg {
		checksum ^= byte(c)
	}
	packet := append(start, msg...)
	packet = append(packet, checksum, 0x03)

	/*
		fmt.Printf("Len: %d checksum: %0X msg: %x packet: %x\n", l, checksum, msg, packet)
		for _, c := range packet {
			if c >= 0x20 && c < 127 {
				fmt.Printf("%c", c)
			} else {
				fmt.Printf(" (%0.2x) ", c)
			}
		}
		fmt.Printf("\n")
	*/
	return packet
}
