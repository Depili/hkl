package main

import (
	"fmt"
	"gitlab.com/Depili/hkl/type1"
	"go.bug.st/serial"
	"log"
	"time"
	_ "time/tzdata"
)

func main() {

	loc, err := time.LoadLocation("Europe/Helsinki")
	if err != nil {
		panic(err)
	}

	addr := []byte("555")
	addr2 := []byte("666")

	msg1 := []byte("T1Clock!")
	p1 := type1.MitronPacket(msg1, addr)
	for _, c := range p1 {
		if c >= 0x20 && c < 127 {
			fmt.Printf("%c", c)
		} else {
			fmt.Printf(" (%0.2x) ", c)
		}
	}
	fmt.Printf("\n")

	msg1 = []byte("T1Clock!")
	p1_2 := type1.MitronPacket(msg1, addr2)
	for _, c := range p1_2 {
		if c >= 0x20 && c < 127 {
			fmt.Printf("%c", c)
		} else {
			fmt.Printf(" (%0.2x) ", c)
		}
	}
	fmt.Printf("\n")

	msg2 := []byte("A1")
	p2 := type1.MitronPacket(msg2, addr)

	for _, c := range p2 {
		if c >= 0x20 && c < 127 {
			fmt.Printf("%c", c)
		} else {
			fmt.Printf(" (%0.2x) ", c)
		}
	}
	fmt.Printf("\n")

	msg2 = []byte("A1")
	p2_2 := type1.MitronPacket(msg2, addr)

	for _, c := range p2_2 {
		if c >= 0x20 && c < 127 {
			fmt.Printf("%c", c)
		} else {
			fmt.Printf(" (%0.2x) ", c)
		}
	}
	fmt.Printf("\n")

	mode := &serial.Mode{
		BaudRate: 2400,
		Parity:   serial.EvenParity,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}

	port, err := serial.Open("/dev/ttymxc2", mode)
	if err != nil {
		log.Fatal(err)
	}

	/*
		c := &serial.Config{Name: "/dev/tty.usbserial-AB0JYFZH", Baud: 2400, Parity: serial.ParityEven}
		port, err := serial.OpenPort(c)
		if err != nil {
			log.Fatal(err)
		}
	*/
	time.Sleep(time.Second)
	port.Write(p1)
	port.Write(p1_2)
	time.Sleep(time.Second)
	port.Write(p2)
	port.Write(p2_2)
	time.Sleep(time.Second)

	i := byte(0)

	for {
		t := time.Now().In(loc).Format("2006-01-02T15:04")

		msg := fmt.Sprintf("T%d%s", i, t)
		p := type1.MitronPacket([]byte(msg), addr)
		port.Write(p)
		time.Sleep(100 * time.Millisecond)
		p = type1.MitronPacket([]byte(msg), addr2)
		port.Write(p)

		time.Sleep(500 * time.Millisecond)

		msg = fmt.Sprintf("A%d", i)
		p = type1.MitronPacket([]byte(msg), addr)
		time.Sleep(100 * time.Millisecond)
		port.Write(p)
		p = type1.MitronPacket([]byte(msg), addr2)
		port.Write(p)

		time.Sleep(2000 * time.Millisecond)
		i = (i + 1) % 10
		fmt.Printf(".")
	}
}
